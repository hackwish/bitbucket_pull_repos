#!/bin/bash
#Script to update from master repositories under a user from bitbucket
#Based and forked by http://haroldsoh.com/2011/10/07/clone-all-repos-from-a-bitbucket-source/

user="user"
password="pass"
local_repo="/destination/folder"
teams="team1 team2 teamN"

date=`date`

echo "Start on: " $date
 
#USER REPOS

curl -u $user:$password https://api.bitbucket.org/1.0/users/$user > repoinfo
 
for repo_name in `cat repoinfo | sed -r 's/("name": )/\n\1/g' | sed -r 's/"name": "(.*)"/\1/' | sed -e 's/{//' | cut -f1 -d\" | tr '\n' ' '`
	do
		echo "Pulling " $repo_name
		cd /var/www/html/$repo_name && git pull origin master
		#cd /var/www/html/$repo_name && git pull origin <other_branches>
		echo "---"
	done

#REPOS TEAMS

for repo_team in $teams

	do
		curl -u $user:$password https://api.bitbucket.org/1.0/users/$repo_team > repoinfoteam

		for repo_name in `cat repoinfoteam | sed -r 's/("name": )/\n\1/g' | sed -r 's/"name": "(.*)"/\1/' | sed -e 's/{//' | cut -f1 -d\" | tr '\n' ' '`
			do
				echo "Pulling " $repo_name
				cd /var/www/html/$repo_name && git pull origin master
			done
	done

exit 0
